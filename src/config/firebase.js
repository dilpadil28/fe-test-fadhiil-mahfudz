// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyBtwZxE1j3qv6czkb2FoVGYe87O9m0YeG4",
  authDomain: "test-uninet.firebaseapp.com",
  projectId: "test-uninet",
  storageBucket: "test-uninet.appspot.com",
  messagingSenderId: "1089512475654",
  appId: "1:1089512475654:web:e5940429597bd513bf8787",
};

const app = initializeApp(firebaseConfig);

export default app;
