import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { ButtonCustom } from "./ButtonCustom";

test("renders ButtonCustom component", () => {
  render(<ButtonCustom>Click me</ButtonCustom>);
  const buttonElement = screen.getByText("Click me");
  expect(buttonElement).toBeInTheDocument();
});

test("handles onClick event", () => {
  const mockOnClick = jest.fn();
  render(<ButtonCustom onClick={mockOnClick}>Click me</ButtonCustom>);
  const buttonElement = screen.getByText("Click me");
  fireEvent.click(buttonElement);
  expect(mockOnClick).toHaveBeenCalled();
});
