import React from "react";
import { render, screen } from "@testing-library/react";
import { TableCustom } from "./TableCustom";
import { TableCell, TableRow } from "@mui/material";
// Mock data for testing
const headCells = ["Header1", "Header2", "Header3"];
const rowData = [
  ["Row1Cell1", "Row1Cell2", "Row1Cell3"],
  ["Row2Cell1", "Row2Cell2", "Row2Cell3"],
];

test("renders TableCustom header cells correctly", () => {
  // Render the component with the provided header cells
  render(<TableCustom headCell={headCells} />);

  // Check if each header cell is present in the document
  headCells.forEach((headerCell) => {
    const headerCellElement = screen.getByText(headerCell);
    expect(headerCellElement).toBeInTheDocument();
  });
});

test("renders TableCustom rows correctly", () => {
  // Render the component with the provided header cells and row data
  render(
    <TableCustom headCell={headCells}>
      {rowData.map((row, rowIndex) => (
        <TableRow key={rowIndex}>
          {row.map((cell, cellIndex) => (
            <TableCell key={cellIndex}>{cell}</TableCell>
          ))}
        </TableRow>
      ))}
    </TableCustom>
  );

  // Check if each cell in the rows is present in the document
  rowData.flat().forEach((cellValue) => {
    const cellElement = screen.getByText(cellValue);
    expect(cellElement).toBeInTheDocument();
  });
});
