import React from "react";
import { render, screen } from "@testing-library/react";
import { Logo } from "./Logo";

test("renders Logo component", () => {
  render(<Logo>LogoText</Logo>);
  const avatarElement = screen.getByText("LogoText");
  expect(avatarElement).toBeInTheDocument();
});
