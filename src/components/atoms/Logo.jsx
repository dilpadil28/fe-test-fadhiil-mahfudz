import Avatar from "@mui/material/Avatar";
export const Logo = (props) => {
  return (
    <Avatar {...props} sx={{ m: 1, bgcolor: "secondary.main" }}>
      {props.children}
    </Avatar>
  );
};
