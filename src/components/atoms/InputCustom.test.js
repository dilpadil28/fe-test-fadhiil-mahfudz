import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { InputCustom } from "./InputCustom";

test("renders InputCustom component", () => {
  render(<InputCustom label="Test Label" />);
  const inputElement = screen.getByLabelText("Test Label");
  expect(inputElement).toBeInTheDocument();
});

test("handles onChange event", () => {
  const mockOnChange = jest.fn();
  render(<InputCustom label="Test Label" onChange={mockOnChange} />);
});
