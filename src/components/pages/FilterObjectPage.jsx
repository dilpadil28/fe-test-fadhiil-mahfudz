import React, { useState } from "react";
import JsonView from "react18-json-view";
import "react18-json-view/src/style.css";
import { dataDummy } from "../../constants/data";
import { Box } from "@mui/material";
import { ButtonCustom } from "../atoms/ButtonCustom";

export const FilterObjectPage = () => {
  const [filtered, setFiltered] = useState([]);

  const handleFilter = () => {
    const data = dataDummy.data.response.billdetails.filter((bill) => {
      const res = parseInt(bill.body[0].split(":")[1]);
      return res >= 100000;
    });
    const val = data.map((bill) => parseInt(bill.body[0].split(":")[1]));
    setFiltered(val);
  };
  return (
    <div>
      <Box m={5}>
        <JsonView src={dataDummy} />
      </Box>
      <Box mx={5} my={2}>
        <Box my={2}>
          <ButtonCustom variant="contained" onClick={handleFilter}>
            Filter Denom {">="} 100000
          </ButtonCustom>
        </Box>
        <JsonView src={filtered} />
      </Box>
    </div>
  );
};
