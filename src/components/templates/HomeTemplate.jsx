import React, { useEffect, useState } from "react";

import { useNavigate } from "react-router-dom";
import {
  getFirestore,
  collection,
  addDoc,
  getDocs,
  updateDoc,
  deleteDoc,
  serverTimestamp,
  doc,
} from "firebase/firestore";
import { TableCell, TableRow, Box, DialogContentText } from "@mui/material";
import { toast } from "react-toastify";
import app from "../../config/firebase";
import { ButtonCustom } from "../atoms/ButtonCustom";
import { TableCustom } from "../atoms/TableCustom";
import ModalCustom from "../molecules/ModalCustom";
import { InputCustom } from "../atoms/InputCustom";

const db = getFirestore(app);

export const HomeTemplate = () => {
  const token = localStorage.getItem("token");
  const navigate = useNavigate();
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [author, setAuthor] = useState("");
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [addModalOpen, setCreateModalOpen] = useState(false);
  const [updateModalOpen, setUpdateModalOpen] = useState(false);
  const [updatePost, setUpdatePost] = useState({
    title: "",
    content: "",
    author: "",
  });
  const [selectedId, setSelectedId] = useState(null);
  const [data, setData] = useState([]);
  useEffect(() => {
    if (!token) navigate("/login");
  }, [navigate, token]);

  const fetchData = async () => {
    const querySnapshot = await getDocs(collection(db, "posts"));
    const newData = querySnapshot.docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    setData(newData);
  };
  useEffect(() => {
    fetchData();
  }, []);

  const handleCreate = async () => {
    try {
      await addDoc(collection(db, "posts"), {
        title,
        content,
        author,
        createdAt: serverTimestamp(),
        updatedAt: serverTimestamp(),
      });
      fetchData();
      setCreateModalOpen(false);
      toast.success("Insert Post Success");
    } catch (error) {
      toast.error(error);
    }
  };

  const handleUpdate = async () => {
    console.log({
      title: updatePost.title,
      content: updatePost.content,
      author: updatePost.author,
      createdAt: serverTimestamp(),
      updatedAt: serverTimestamp(),
    });
    try {
      await updateDoc(doc(db, "posts", selectedId), {
        title: updatePost.title,
        content: updatePost.content,
        author: updatePost.author,
        createdAt: serverTimestamp(),
        updatedAt: serverTimestamp(),
      });
      fetchData();
      setUpdateModalOpen(false);
      setSelectedId(null);
      toast.success("Update Post Success");
    } catch (error) {
      toast.error(error);
    }
  };

  const handleDelete = (id) => {
    setDeleteModalOpen(true);
    setSelectedId(id);
  };

  const handleConfirmDelete = async () => {
    try {
      if (selectedId) {
        await deleteDoc(doc(db, "posts", selectedId));
        fetchData();
        toast.success("Delete Post Success");
        setDeleteModalOpen(false);
        setSelectedId(null);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleCancelDelete = () => {
    setDeleteModalOpen(false);
    setSelectedId(null);
  };

  return (
    <Box m={5}>
      <Box mb={3}>
        <ButtonCustom
          variant="contained"
          onClick={() => setCreateModalOpen(true)}
        >
          Create
        </ButtonCustom>
      </Box>
      <TableCustom headCell={["Title", "Content", "Author", "Actions"]}>
        {data.length > 0 ? (
          data.map((post) => (
            <TableRow key={post.id}>
              <TableCell>{post.title}</TableCell>
              <TableCell>{post.content}</TableCell>
              <TableCell>{post.author}</TableCell>
              <TableCell>
                <ButtonCustom
                  onClick={() => {
                    setSelectedId(post.id);
                    setUpdatePost({
                      title: post.title,
                      content: post.content,
                      author: post.author,
                    });
                    setUpdateModalOpen(true);
                  }}
                >
                  Update
                </ButtonCustom>
                <ButtonCustom onClick={() => handleDelete(post.id)}>
                  Delete
                </ButtonCustom>
              </TableCell>
            </TableRow>
          ))
        ) : (
          <Box m={5}>No Data</Box>
        )}
      </TableCustom>

      <ModalCustom
        open={updateModalOpen}
        onClose={() => setUpdateModalOpen(false)}
        onSubmit={handleUpdate}
        title="Update Post"
        buttonText="Update"
      >
        <InputCustom
          onChange={(e) => {
            setUpdatePost({ ...updatePost, title: e.target.value });
          }}
          defaultValue={updatePost.title}
          margin="normal"
          required
          fullWidth
          id="title"
          label="Title"
          name="title"
          autoComplete="title"
        />
        <InputCustom
          onChange={(e) => {
            setUpdatePost({ ...updatePost, content: e.target.value });
          }}
          defaultValue={updatePost.content}
          margin="normal"
          required
          fullWidth
          id="content"
          label="Content"
          name="content"
          autoComplete="content"
          multiline
          rows={4}
        />
        <InputCustom
          onChange={(e) => {
            setUpdatePost({ ...updatePost, author: e.target.value });
          }}
          defaultValue={updatePost.author}
          margin="normal"
          required
          fullWidth
          id="author"
          label="Author"
          name="author"
          autoComplete="author"
        />
      </ModalCustom>
      <ModalCustom
        open={deleteModalOpen}
        onClose={handleCancelDelete}
        onSubmit={handleConfirmDelete}
        title="Delete Post"
        buttonText="Delete"
      >
        <DialogContentText>
          Are you sure you want to delete this post?
        </DialogContentText>
      </ModalCustom>
      <ModalCustom
        open={addModalOpen}
        onClose={() => setCreateModalOpen(false)}
        onSubmit={handleCreate}
        title="Create Post"
        buttonText="Create"
      >
        <InputCustom
          onChange={(e) => {
            setTitle(e.target.value);
          }}
          margin="normal"
          required
          fullWidth
          id="title"
          label="Title"
          name="title"
          autoComplete="title"
        />
        <InputCustom
          onChange={(e) => {
            setContent(e.target.value);
          }}
          margin="normal"
          required
          fullWidth
          id="content"
          label="Content"
          name="content"
          autoComplete="content"
          multiline
          rows={4}
        />
        <InputCustom
          onChange={(e) => {
            setAuthor(e.target.value);
          }}
          margin="normal"
          required
          fullWidth
          id="author"
          label="Author"
          name="author"
          autoComplete="author"
        />
      </ModalCustom>
    </Box>
  );
};
