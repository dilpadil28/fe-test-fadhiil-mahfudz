import React from "react";
import { Outlet } from "react-router-dom";
import { Header } from "../organisms/Header";

export const Layout = () => {
  return (
    <div>
      <Header />
      <Outlet />
    </div>
  );
};
