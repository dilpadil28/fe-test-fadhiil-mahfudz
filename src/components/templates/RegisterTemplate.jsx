import React, { useEffect } from "react";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { Logo } from "../atoms/Logo";
import { FormSign } from "../molecules/FormSign";
import axios from "axios";
import { toast } from "react-toastify";

import { useNavigate } from "react-router-dom";

export const RegisterTemplate = () => {
  let navigate = useNavigate();
  const token = localStorage.getItem("token");
  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    try {
      const res = await axios.post(" https://reqres.in/api/register", {
        email: data.get("email"),
        password: data.get("password"),
      });
      localStorage.setItem("token", res.data.token);
      navigate("/");
    } catch (error) {
      toast.error(error.response.data.error);
    }
  };

  useEffect(() => {
    if (token) navigate("/");
  }, [navigate, token]);

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Logo>
          <LockOutlinedIcon />
        </Logo>
        <Typography component="h1" variant="h5">
          Register
        </Typography>
        <FormSign handleSubmit={handleSubmit} type="register" />
      </Box>
    </Container>
  );
};
