import React from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from "@mui/material";
import { ButtonCustom } from "../atoms/ButtonCustom";

const ModalCustom = ({
  open,
  onClose,
  onSubmit,
  title,
  buttonText,
  children,
}) => {
  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>{children}</DialogContent>
      <DialogActions>
        <ButtonCustom onClick={onClose}>Cancel</ButtonCustom>
        <ButtonCustom onClick={onSubmit} variant="contained">
          {buttonText}
        </ButtonCustom>
      </DialogActions>
    </Dialog>
  );
};

export default ModalCustom;
