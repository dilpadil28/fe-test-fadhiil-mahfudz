import React from "react";
import { render, screen } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import { FormSign } from "./FormSign";

test("renders FormSign component with login type", () => {
  render(
    <Router>
      <FormSign handleSubmit={() => {}} type="login" />
    </Router>
  );

  const loginButton = screen.getByRole("button", { name: "Login" });
  const registerLink = screen.getByRole("link", {
    name: "Don't have an account? Register",
  });

  expect(loginButton).toBeInTheDocument();
  expect(registerLink).toBeInTheDocument();
});
