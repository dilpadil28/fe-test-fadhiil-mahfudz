import React from "react";

import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import { InputCustom } from "../atoms/InputCustom";
import { ButtonCustom } from "../atoms/ButtonCustom";

export const FormSign = ({ handleSubmit, type = "login" }) => {
  return (
    <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
      <InputCustom
        margin="normal"
        required
        fullWidth
        id="email"
        label="Email Address"
        name="email"
        autoComplete="email"
        autoFocus
      />
      <InputCustom
        margin="normal"
        required
        fullWidth
        name="password"
        label="Password"
        type="password"
        id="password"
        autoComplete="current-password"
      />
      <ButtonCustom
        type="submit"
        fullWidth
        variant="contained"
        sx={{ mt: 3, mb: 2 }}
      >
        {type === "login" ? "Login" : "Register"}
      </ButtonCustom>
      <Grid container>
        <Grid item>
          {type === "login" ? (
            <Link to={"/register"} variant="body2">
              {"Don't have an account? Register"}
            </Link>
          ) : (
            <Link to={"/login"} variant="body2">
              {"Already have an account? Login"}
            </Link>
          )}
        </Grid>
      </Grid>
    </Box>
  );
};
