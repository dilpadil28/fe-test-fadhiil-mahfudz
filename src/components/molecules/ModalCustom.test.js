import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import ModalCustom from "./ModalCustom";

test("renders ModalCustom component", () => {
  render(
    <ModalCustom
      open={true}
      onClose={() => {}}
      onSubmit={() => {}}
      title="Test Modal"
      buttonText="Submit"
    >
      <p>Modal content</p>
    </ModalCustom>
  );

  const titleElement = screen.getByText("Test Modal");
  const contentElement = screen.getByText("Modal content");
  const cancelButton = screen.getByRole("button", { name: "Cancel" });
  const submitButton = screen.getByRole("button", { name: "Submit" });

  expect(titleElement).toBeInTheDocument();
  expect(contentElement).toBeInTheDocument();
  expect(cancelButton).toBeInTheDocument();
  expect(submitButton).toBeInTheDocument();
});

test("calls onClose and onSubmit functions on button clicks", () => {
  const mockOnClose = jest.fn();
  const mockOnSubmit = jest.fn();

  render(
    <ModalCustom
      open={true}
      onClose={mockOnClose}
      onSubmit={mockOnSubmit}
      title="Test Modal"
      buttonText="Submit"
    >
      <p>Modal content</p>
    </ModalCustom>
  );

  const cancelButton = screen.getByRole("button", { name: "Cancel" });
  const submitButton = screen.getByRole("button", { name: "Submit" });

  fireEvent.click(cancelButton);
  fireEvent.click(submitButton);

  expect(mockOnClose).toHaveBeenCalled();
  expect(mockOnSubmit).toHaveBeenCalled();
});
